using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicPlayerController : MonoBehaviour
{
    [Header("Ground Check")]
    [SerializeField] private float _groundCheckRadius = 0.3f;
    [SerializeField] private Vector2 _groundCheckOffset;

    [Header("Horizontal Movement")]
    [SerializeField] private float _baseSpeed = 7f;
    [SerializeField] private float _acceleration = 100f;
    [SerializeField] private float _deceleration = 100f;

    [Header("Jump")]
    [SerializeField] private float _jumpInitialSpeed = 15f;
    [SerializeField] private float _jumpBufferGraceDuration = 0.15f;
    [SerializeField] private float _coyoteGraceDuration = 0.15f;
    [SerializeField] private int _airJumpsMax = 0;
    [SerializeField] private float _jumpTimeMin = 0.1f;
    [SerializeField] private float _jumpCancelingFactor = 0f;

    private Rigidbody2D _rb;

    private float _dt;

    private bool _grounded;
    private float _horizontalInput;
    private bool _jumpHeld;

    private bool _jumpRequested;
    private float _jumpRequestTime = float.MinValue;
    private float _timeLeftGround = float.MinValue;
    private bool _coyoteUsable = false;
    private int _airJumpsRemaining = 0;
    private bool _jumpingUp;
    private bool _jumpCanceled;
    private float _lastJumpTime = float.MinValue;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        GatherInput();
    }

    private void GatherInput()
    {
        _horizontalInput = Input.GetAxisRaw("Horizontal");
        if (_horizontalInput != 0f)
        {
            _horizontalInput /= Mathf.Abs(_horizontalInput);
        }

        if (Input.GetButtonDown("Jump"))
        {
            RequestJump();
        }

        _jumpHeld = Input.GetButton("Jump");
    }

    private void RequestJump()
    {
        _jumpRequested = true;
        _jumpRequestTime = Time.time;
    }

    private void FixedUpdate()
    {
        _dt = Time.deltaTime;

        CheckGround();
        CalculateJump();
        CheckJumpCanceling();
        Move();

        CleanFrameData();
    }

    private void CheckGround()
    {
        bool foundGround = Physics2D.OverlapCircle((Vector2)transform.position + _groundCheckOffset, _groundCheckRadius, Physics2D.GetLayerCollisionMask(gameObject.layer));
        if (foundGround != _grounded)
        {
            ToggleGround(foundGround);
        }
    }

    private void ToggleGround(bool isGrounded)
    {
        _grounded = isGrounded;

        if (_grounded)
        {
            _coyoteUsable = true;
            _airJumpsRemaining = _airJumpsMax;
            _jumpingUp = false;
        }
        else
        {
            _timeLeftGround = Time.time;
        }
    }

    private void CalculateJump()
    {
        if (_jumpingUp && _rb.velocity.y < 0f)
        {
            _jumpingUp = false;
        }

        if (_jumpRequested)
        {
            if (_grounded)
            {
                ExecuteJump();
            }
            else if (_coyoteUsable && Time.time < _timeLeftGround + _coyoteGraceDuration)
            {
                ExecuteJump();
            }
            else if (_airJumpsRemaining > 0)
            {
                ExecuteAirJump();
            }
        }
    }

    private void ExecuteJump()
    {
        _jumpRequested = false;
        _coyoteUsable = false;
        _jumpingUp = true;
        _jumpCanceled = false;
        _lastJumpTime = Time.time;
        _rb.velocity = new Vector2(_rb.velocity.x, _jumpInitialSpeed);
    }

    private void ExecuteAirJump()
    {
        _airJumpsRemaining--;
        ExecuteJump();
    }

    private void CheckJumpCanceling()
    {
        if (!_jumpingUp) return;
        if (_jumpCanceled) return;
        if (Time.time < _lastJumpTime + _jumpTimeMin) return;

        if (!_jumpHeld)
        {
            _jumpCanceled = true;
            if (_rb.velocity.y > 0f)
            {
                _rb.velocity = new Vector2(_rb.velocity.x, _rb.velocity.y * _jumpCancelingFactor);
            }
        }
    }

    private void Move()
    {
        float targetHorizontalVelocity = _horizontalInput * _baseSpeed;
        float step = targetHorizontalVelocity == 0f ? _deceleration : _acceleration;
        step *= _dt;

        float frameHorizontalVelocity = Mathf.MoveTowards(_rb.velocity.x, targetHorizontalVelocity, step);

        _rb.velocity = new Vector2(frameHorizontalVelocity, _rb.velocity.y);
    }

    private void CleanFrameData()
    {
        if (Time.time > _jumpRequestTime + _jumpBufferGraceDuration)
        {
            _jumpRequested = false;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + (Vector3)_groundCheckOffset, _groundCheckRadius);
    }
}
