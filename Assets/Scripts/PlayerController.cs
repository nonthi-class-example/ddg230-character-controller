using UnityEngine;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private float _characterWidth = 0.8f;
    [SerializeField] private float _characterHeight = 1.6f;
    [SerializeField] private float _gravityScale = 3f;
    [SerializeField] private float _grounedRayInset = 0.1f;

    [Header("Horizontal Movement")]
    [SerializeField] private float _baseSpeed = 9f;
    [SerializeField] private float _acceleration = 100f;
    [SerializeField] private float _deceleration = 100f;
    [SerializeField] private float _maxWalkableSlope = 50f;
    [SerializeField] private float _characterStepHeight = 0.6f;

    [Header("Jump")]
    [SerializeField] private float _jumpInitialSpeed = 15f;
    [SerializeField] private float _jumpBufferGraceDuration = 0.15f;
    [SerializeField] private float _coyoteGraceDuration = 0.15f;
    [SerializeField] private int _airJumpsMax = 0;
    [SerializeField] private float _jumpTimeMin = 0.1f;
    [SerializeField] private float _jumpCancelingFactor = 0f;

    private const float SKIN_WIDTH = 0.02f;
    private const float COLLIDER_EDGE_RADIUS = 0.05f;
    private const int RAY_SIDE_COUNT = 5;

    private Rigidbody2D _rb;

    private BoxCollider2D _groundedCollider;
    private CapsuleCollider2D _airCollider;

    private Vector2 _standingColliderCenter;
    private Vector2 _standingColliderSize;

    private Vector2 _airColliderCenter;
    private Vector2 _airColliderSize;

    private float _dt;
    private Vector2 _moveInput;
    private bool _hasHorizontalInputThisFrame;
    private bool _jumpHeld;

    private Vector2 GrounderOriginPoint => _framePosition + _up * (_characterStepHeight + SKIN_WIDTH);
    private float GrounderLength => _characterStepHeight + SKIN_WIDTH;
    private RaycastHit2D _groundHit;
    [SerializeField] private bool _grounded;
    private float _currentStepDownLength;

    private Vector2 _framePosition;
    [SerializeField] private Vector2 _frameDirection;
    private Vector2 _velocity;
    private Vector2 _trimmedVelocity;
    private Vector2 _up;
    private Vector2 _right;

    private Vector2 _transcientVelocity;
    private Vector2 _totalTranscientVelocityAppliedLastFrame;

    private Vector2 _forceToApplyThisFrame;

    private const float JUMP_CLEARANCE_TIME = 0.25f;
    private bool IsWithinJumpClearance => _lastJumpTime + JUMP_CLEARANCE_TIME > Time.time;
    private bool _jumpRequested;
    private float _jumpRequestTime = float.MinValue;
    private float _timeLeftGround = float.MinValue;
    private bool _coyoteUsable = false;
    private int _airJumpsRemaining = 0;
    private bool _jumpingUp;
    private bool _jumpCanceled;
    private float _lastJumpTime = float.MinValue;

    public void AddFrameForce(Vector2 force, bool resetVelocity = false)
    {
        if (resetVelocity) SetVelocity(Vector2.zero);
        _forceToApplyThisFrame += force;
    }

    private void Awake()
    {
        SetupCharacter();
    }

    private void OnValidate()
    {
        SetupCharacter();
    }

    private void Update()
    {
        ReceiveInput();
    }

    private void FixedUpdate()
    {
        _dt = Time.deltaTime;

        RemoveTransientVelocity();
        SetFrameData();
        CheckCollisions();
        CalculateDirection();

        CalculateJump();

        CheckSnapToGround();

        CheckJumpCanceling();
        Move();

        CleanFrameData();
    }

    private void SetupCharacter()
    {
        _rb = GetComponent<Rigidbody2D>();
        _groundedCollider = GetComponent<BoxCollider2D>();
        _airCollider = GetComponent<CapsuleCollider2D>();

        _rb.gravityScale = _gravityScale;

        _standingColliderSize = new Vector2(_characterWidth - 2 * COLLIDER_EDGE_RADIUS, _characterHeight - _characterStepHeight - 2 * COLLIDER_EDGE_RADIUS);
        _standingColliderCenter = new Vector2(0, _characterHeight - _standingColliderSize.y / 2 - COLLIDER_EDGE_RADIUS);

        _airColliderSize = new Vector2(_characterWidth - 2 * SKIN_WIDTH, _characterHeight - 2 * SKIN_WIDTH);
        _airColliderCenter = new Vector2(0, _characterHeight - _airColliderSize.y / 2);

        _groundedCollider.sharedMaterial = _rb.sharedMaterial;
        _airCollider.sharedMaterial = _rb.sharedMaterial;

        AdjustColliders();
    }

    private void AdjustColliders()
    {
        _groundedCollider.size = _standingColliderSize;
        _groundedCollider.offset = _standingColliderCenter;
        _groundedCollider.edgeRadius = COLLIDER_EDGE_RADIUS;

        _airCollider.size = _airColliderSize;
        _airCollider.offset = _airColliderCenter;

        if (_grounded)
        {
            _airCollider.enabled = false;
            _groundedCollider.enabled = true;
        }
        else
        {
            _airCollider.enabled = true;
            _groundedCollider.enabled = false;
        }
    }

    private void ReceiveInput()
    {
        _moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (Input.GetButtonDown("Jump"))
        {
            RequestJump();
        }

        _jumpHeld = Input.GetButton("Jump");
    }

    private void RequestJump()
    {
        _jumpRequested = true;
        _jumpRequestTime = Time.time;
    }

    private void RemoveTransientVelocity()
    {
        var currentVelocity = _rb.velocity;
        currentVelocity -= _totalTranscientVelocityAppliedLastFrame;

        SetVelocity(currentVelocity);

        _transcientVelocity = Vector2.zero;
        _totalTranscientVelocityAppliedLastFrame = Vector2.zero;
    }

    private void SetFrameData()
    {
        _framePosition = _rb.position;
        _velocity = _rb.velocity;
        _up = transform.up;
        _right = transform.right;

        _hasHorizontalInputThisFrame = _moveInput.x != 0f;

        _trimmedVelocity = new Vector2(_velocity.x, 0f);
    }

    private void CheckCollisions()
    {
        bool groundedThisFrame = PerformGroundRay(GrounderOriginPoint);

        if (!groundedThisFrame)
        {
            foreach (var offset in GenerateRayOffsets())
            {
                groundedThisFrame = PerformGroundRay(GrounderOriginPoint + _right * offset) || PerformGroundRay(GrounderOriginPoint - _right * offset);
                if (groundedThisFrame) break;
            }
        }

        if (groundedThisFrame != _grounded)
        {
            ToggleGrounded(groundedThisFrame);
        }
    }

    private bool PerformGroundRay(Vector2 origin)
    {
        _groundHit = Physics2D.Raycast(origin, -_up, _currentStepDownLength + GrounderLength, Physics2D.GetLayerCollisionMask(_rb.gameObject.layer));
        if (!_groundHit) return false;

        if (Vector2.Angle(_groundHit.normal, _up) > _maxWalkableSlope)
        {
            return false;
        }

        return true;
    }

    private IEnumerable<float> GenerateRayOffsets()
    {
        var extent = _standingColliderSize.x / 2 - _grounedRayInset;
        var offsetAmount = extent / RAY_SIDE_COUNT;
        for (var i = 1; i < RAY_SIDE_COUNT + 1; i++)
        {
            yield return offsetAmount * i;
        }
    }

    private void ToggleGrounded(bool grounded)
    {
        _grounded = grounded;
        if (grounded)
        {
            SetVelocity(_trimmedVelocity);
            _currentStepDownLength = _characterStepHeight;
            _rb.gravityScale = 0f;

            _coyoteUsable = true;
            _airJumpsRemaining = _airJumpsMax;
            _jumpingUp = false;
        }
        else
        {
            _rb.gravityScale = _gravityScale;

            _timeLeftGround = Time.time;
        }

        AdjustColliders();
    }

    private void CalculateDirection()
    {
        _frameDirection = new Vector2(_moveInput.x, 0f);

        if (_grounded)
        {
            _frameDirection.y = _frameDirection.x * -_groundHit.normal.x / _groundHit.normal.y;
        }

        _frameDirection = _frameDirection.normalized;
    }

    private void CalculateJump()
    {
        if (_jumpingUp && _velocity.y < 0f)
        {
            _jumpingUp = false;
        }

        if (_jumpRequested)
        {
            if (_grounded)
            {
                ExecuteJump();
            }
            else if (_coyoteUsable && Time.time < _timeLeftGround + _coyoteGraceDuration)
            {
                ExecuteJump();
            }
            else if (_airJumpsRemaining > 0)
            {
                ExecuteAirJump();
            }
        }
    }

    private void ExecuteJump()
    {
        _currentStepDownLength = 0f;
        _jumpRequested = false;
        _coyoteUsable = false;
        _jumpingUp = true;
        _jumpCanceled = false;
        _lastJumpTime = Time.time;

        SetVelocity(_trimmedVelocity);
        AddFrameForce(new Vector2(0, _jumpInitialSpeed));
    }

    private void ExecuteAirJump()
    {
        _airJumpsRemaining--;
        ExecuteJump();
    }

    private void CheckSnapToGround()
    {
        if (_grounded && !IsWithinJumpClearance)
        {
            var distanceFromGround = _characterStepHeight - _groundHit.distance;
            if (distanceFromGround != 0f)
            {
                var requiredMove = new Vector2(0, distanceFromGround);
                _transcientVelocity = requiredMove / _dt;
            }
        }
    }

    private void CheckJumpCanceling()
    {
        if (!_jumpingUp) return;
        if (_jumpCanceled) return;
        if (Time.time < _lastJumpTime + _jumpTimeMin) return;

        if (!_jumpHeld)
        {
            _jumpCanceled = true;
            if (_velocity.y > 0f)
            {
                SetVelocity(new Vector2(_velocity.x, _velocity.y * _jumpCancelingFactor));
            }
        }
    }

    private void Move()
    {
        if (_forceToApplyThisFrame != Vector2.zero)
        {
            _rb.velocity += CalculateAdditionalFrameVelocities();
            _rb.AddForce(_forceToApplyThisFrame * _rb.mass, ForceMode2D.Impulse);
            return;
        }

        Vector2 newVelocity = _velocity;

        var targetSpeed = _hasHorizontalInputThisFrame ? _baseSpeed : 0f;
        var step = _hasHorizontalInputThisFrame ? _acceleration : _deceleration;
        step *= _dt;

        if (_grounded && !IsWithinJumpClearance)
        {
            var previousHorizontalVelocity = Vector2.Dot(_velocity, _frameDirection);
            newVelocity = Mathf.MoveTowards(previousHorizontalVelocity, targetSpeed, step) * _frameDirection;
        }
        else
        {
            newVelocity = new Vector2(Mathf.MoveTowards(_trimmedVelocity.x, _frameDirection.x * targetSpeed, step), _velocity.y);
        }

        SetVelocity(newVelocity + CalculateAdditionalFrameVelocities());
    }

    private Vector2 CalculateAdditionalFrameVelocities()
    {
        _totalTranscientVelocityAppliedLastFrame = _transcientVelocity;
        return _totalTranscientVelocityAppliedLastFrame;
    }

    private void SetVelocity(Vector2 newVel)
    {
        _rb.velocity = newVel;
        _velocity = newVel;
    }

    private void CleanFrameData()
    {
        _forceToApplyThisFrame = Vector2.zero;

        if (Time.time > _jumpRequestTime + _jumpBufferGraceDuration)
        {
            _jumpRequested = false;
        }
    }

    private void OnDrawGizmos()
    {
        Vector2 pos = transform.position;
        Vector2 up = transform.up;
        Vector2 right = transform.right;
        Vector2 rayStart = pos + (GrounderLength * up);
        Vector2 rayDir = -(_currentStepDownLength + GrounderLength) * up;

        Gizmos.color = Color.magenta;
        Gizmos.DrawRay(rayStart, rayDir);
        foreach (var offset in GenerateRayOffsets())
        {
            Gizmos.DrawRay(rayStart + right * offset, rayDir);
            Gizmos.DrawRay(rayStart - right * offset, rayDir);
        }
    }
}
